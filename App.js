import logo from './logo.svg';
import './App.css';
import React from 'react';
import useSWR from 'swr';
function App() {

  const fetcher=(...args)=>fetch(...args).then((res)=>res.json());
  const Swr=()=>{
    const{data:countries,error,isValidating}=useSWR('https://restcountries.com/v2/all',fetcher)
    
    if(error) return <div className='failed'>Faild to load</div>
    if(isValidating) return <div className='Loading'>Loading...</div>

    return (
      <div>
        {countries && countries.map((country,index)=>(
          <img key={index} src={country.flags.png} alt='flag' width={100}></img>
        ))}
      </div>
    );
  }  
  return <div><Swr> </Swr></div>
}
export default App;
